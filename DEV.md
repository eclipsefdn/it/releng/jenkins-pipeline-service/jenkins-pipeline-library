## Dev notes

## How to update?

### With jsonnet changes in `git@gitlab.eclipse.org:eclipsefdn/it/releng/kube-deploy.git`

in `resources/org/eclipsefdn/jamstack/deployment`

```
$ jb update
```

to update the `jsonnet.lock.json`.

Commit the change

### Changes in jenkins-pipeline-library

(including when `jsonnet.lock.json` has been updated)

```
$ git tag -m "v0.9.9" v0.9.9
$ git push --tags
```

* update Jenkins configuration, set global shared libraries version to new tag
  * https://foundation.eclipse.org/ci/webdev/configure
* update webdev's Jiro to new tag (and deploy config)
  * https://github.com/eclipse-cbi/jiro/blob/master/instances/foundation-internal.webdev/jenkins/configuration.yml#L50-L54


## How to dev?

* Create a test MR/PR on a project
* Use Jenkins' *replay* feature to use your dev branch, e.g.

```
@Library('releng-pipeline@my_dev_branch') _

hugo (
  appName: 'edgenative.eclipse.org',
  productionDomain: 'edgenative.eclipse.org',
)
```

Note the `@my_dev_branch` above (can also be `@main` if needed).

Note that **only `@main` is not cached by Jenkins**. Change Jenkins shared library config to modify that.