local jamstack = import "vendor/gitlab.eclipse.org/eclipsefdn/it/releng/kube-deploy/jamstack/jamstack.libsonnet";
local metadata = import "vendor/gitlab.eclipse.org/eclipsefdn/it/releng/kube-deploy/utils/metadata.libsonnet";

// local config = {
//     "appName": "lts.eclipse.org",
//     "safeAppName": "lts-eclipse-org",
//     "productionBranchName": "main",
//     "productionDomain": "lts.eclipse.org",
//     "deployPreviews": true,
//     "previewDomain": "eclipsecontent.org",
//     "previewBranchesRegex": null,
//     "branchDomain": null,
//     "debug": false,
//     "container": {
//         "name": "docker.io/eclipsefdn/lts-eclipse-org",
//         "version": "mr-24--2208996-b85",
//         "dockerfile": null,
//         "registryCredentialsId": "04264967-fea0-40c2-bf60-09af5aeba60f"
//     },
//     "deployment": {
//         "locationPath": "/",
//         "nginxHttpPort": 8080,
//         "nginxServerConf": null,
//         "dateTimeUTC": "2022-06-18T20:09:16.558296572",
//         "domain": "preview-24--lts-eclipse-org.eclipsecontent.org",
//         "kind": "changeRequestPreview",
//     },
//     "build": {
//         "kubeAgentYmlFile": null,
//         "containerImage": "eclipsefdn/hugo-node:latest",
//         "script": null,
//         "destinationFolder": "./public/"
//     },
//     "kubernetes": {
//         "namespace": "foundation-internal-webdev-jamstack-sites",
//         "credentialsId": "ci-bot-okd-c1-token",
//         "apiServerUrl": "https://api.okd-c1.eclipse.org:6443",
//         "deploymentName": "lts-eclipse-org--mr-24"
//     },
//     "scm": {
//         "credentialsId": "gitlab-api-token3",
//         "changeId": "24",
//         "branchName": "MR-24",
//         "repoHost": "gitlab.eclipse.org",
//         "repoId": "eclipsefdn/it/websites/lts.eclipse.org",
//         "botId": "webdev-bot",
//         "commitId": "2208996b450a0d45ccb5d5f270c0bb4192218ec8"
//     }
// };

function(config)
{
  jamstack::
    jamstack.newDeployment(
      metadata.newMetadata(
        config.safeAppName,
        config.kubernetes.deploymentName,
        config.kubernetes.namespace,
        config.scm.branchName,
        config.deployment.kind,
        config.scm.botId,
        config.deployment.dateTimeUTC,
        config.scm.repoHost,
        config.scm.repoId,
        config.scm.changeId
      ),
      config.deployment.containerImageNameWithTag,
      config.deployment.domain,
      config.deployment.nginxHttpPort,
      config.deployment.locationPath,
      config.deployment.locationPath,
      if (config.deployment.authBasic) then config.deployment.authBasicSecretPath else config.deployment.secretPath,
    ),
  apiVersion: "v1",
  kind: "List",
  items: [
    $.jamstack.deployment,
    $.jamstack.service,
    $.jamstack.route,
  ]
}