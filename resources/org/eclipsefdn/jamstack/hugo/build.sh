#!/usr/bin/env bash

set -euo pipefail

if [[ -e yarn.lock ]]; then
  yarn install --frozen-lockfile;
elif [[ -e package-lock.json ]]; then
  npm --unsafe-perm ci;
fi;

hugo --gc --minify --destination "${1}" --baseURL "${2}"