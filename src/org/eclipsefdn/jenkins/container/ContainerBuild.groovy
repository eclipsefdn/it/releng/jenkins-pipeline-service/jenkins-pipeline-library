package org.eclipsefdn.jenkins.container

import org.jenkinsci.plugins.workflow.cps.CpsScript
import java.util.regex.Matcher

/**
 * Represents a container in the Jenkins pipeline for building and managing Docker images.
 * This class provides methods for interacting with Docker registries and building/publishing Docker images.
 */
@SuppressWarnings(['SerializableClassMustDefineSerialVersionUID', 'CompileStatic'])
class ContainerBuild implements Serializable {

  /**
    * The CpsScript associated with the Jenkins workflow.
    */
  CpsScript workflowScript

   /**
    * The address of the BuildKit daemon. Defaults to "tcp://buildkitd.foundation-internal-infra-buildkitd:1234".
    */
  String buildkitdAddr

  /**
    * Constructor for the ContainerBuild class.
    *
    * @param workflowScript The CpsScript associated with the Jenkins workflow.
    * @param buildkitdAddr The address of the BuildKit daemon.
    * Defaults to "tcp://buildkitd.foundation-internal-infra-buildkitd:1234".
    */
  ContainerBuild(
    CpsScript workflowScript,
    String buildkitdAddr = "tcp://buildkitd.foundation-internal-infra-buildkitd:1234") {

    this.workflowScript = workflowScript
    this.buildkitdAddr = buildkitdAddr
    }

  /**
    * Login to a Docker registry using the specified credential.
    *
    * @param credentialsId The ID of the Jenkins credential containing Docker registry credentials.
    * @param registry The Docker registry URL. Defaults to 'docker.io'.
    * @param name The registry name. Take precedence over registry param if a registry is found in the name.
    * @param debug Enable debugging if set to 'true'.
    * @throws IllegalArgumentException if 'credentialsId' is not provided.
    */
  void login(String credentialsId, String registry = 'docker.io', String name = '', boolean debug = false) {
    if (!credentialsId?.trim()) {
      throw new IllegalArgumentException("Parameter 'credentialsId' is mandatory")
    }

    String craneAuthRegistry = registry
    Matcher registryNamespaceMatcher = (name =~ '^(([^/]+)/)?([^/]+)/([^/:]+)(:.*)?$')

    if (registryNamespaceMatcher.matches()) {
      craneAuthRegistry = registryNamespaceMatcher[0][2]
    }

    if (!craneAuthRegistry?.trim()) {
      craneAuthRegistry = 'docker.io'
    }

    workflowScript.withCredentials([workflowScript.usernamePassword(
      credentialsId: credentialsId,
      passwordVariable: 'DOCKER_REGISTRY_PASSWORD',
      usernameVariable: 'DOCKER_REGISTRY_USERNAME')]) {
        workflowScript.sh """#!/usr/bin/env bash
          set -euo pipefail

          [[ "${debug}" == "true" ]] && set -x
          echo "Access registry "${craneAuthRegistry}" for "${name}" with user: "\${DOCKER_REGISTRY_USERNAME}""

          crane auth login "${craneAuthRegistry}" \
            --password-stdin --username "\${DOCKER_REGISTRY_USERNAME}" <<<"\${DOCKER_REGISTRY_PASSWORD}"
        """
      }
  }

  /**
    * Build and publish a Docker image.
    *
    * @param name The name of the Docker image.
    * @param dockerfile The path to the Dockerfile. Defaults to 'Dockerfile'.
    * @param context The build context directory. Defaults to '.'.
    * @param push Push the image to the registry if set to 'true', default 'true'.
    * @param buildArgs Add some extra Args to build container, default empty value.
    * @param annotation Allows to set default annotations to container depending on build env if set to 'true', default 'true'
    * @param latest Tag the version also as latest.
    * @param debug Enables debugging if set to 'true'.
    * @throws IllegalArgumentException if 'name' is not provided.
    */
  @SuppressWarnings(['BuilderMethodWithSideEffects', 'FactoryMethodName'])
  void build(
    String name,
    String version = 'latest',
    List<String> extraVersions = [],
    String aliases = '',
    String dockerfile = 'Dockerfile',
    String context = '.',
    boolean push = true,
    String buildArgs = '',
    boolean annotation = true,
    boolean latest = false,
    boolean debug = false) {

    if (!name?.trim()) {
      throw new IllegalArgumentException("Parameter 'name' is mandatory")
    }
    String containerNameWithExtraVersion = ""
    if(extraVersions){
      containerNameWithExtraVersion = extraVersions.collect { extraVersion -> "${name}:${extraVersion}" }.join(',')
    }
    workflowScript.sh """#!/usr/bin/env bash
      set -eo pipefail

      [[ "${debug}" == "true" ]] && set -x

      IMAGE_ANNOTATIONS=""
      if [[ "${annotation}" == "true" ]]; then
        BUILD_DATE="\$(date -u +"%Y-%m-%dT%H:%M:%SZ")"
        IMAGE_ANNOTATIONS+=","
        IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.created=\$BUILD_DATE,"
        [[ -n "\$CI_IMAGE_AUTHORS" ]] && IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.authors=\${CI_IMAGE_AUTHORS:-\$BUILD_USER},"
        [[ -n "\$CI_IMAGE_TITLE" ]] && IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.description=\$BUILD_TITLE,"
        [[ -n "\$CI_PROJECT_URL" ]] && IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.documentation=\$CI_PROJECT_URL,"
        [[ -n "\$CI_PROJECT_LICENSE" ]] && IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.licenses=\$CI_PROJECT_LICENSE,"
        IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.ref.name=\${CI_IMAGE_REF_NAME:-${name}:${version}},"
        IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.revision=\${CI_IMAGE_REVISION:-\$GIT_BASE_COMMIT},"
        IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.source=\${CI_IMAGE_SOURCE:-\$GIT_URL},"
        [[ -n "\$CI_IMAGE_TITLE" ]] && IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.title=\$CI_IMAGE_TITLE,"
        IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.url=\${CI_IMAGE_URL:-\$GIT_URL},"
        [[ -n "\$CI_IMAGE_VENDOR" ]] && IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.vendor=\$CI_IMAGE_VENDOR,"
        IMAGE_ANNOTATIONS+="annotation.org.opencontainers.image.version=${version}"
      fi

      BUILD_IMAGE_NAME="${name}:${version}"

      if [[ -n "${containerNameWithExtraVersion}" ]]; then
        BUILD_IMAGE_NAME+=",${containerNameWithExtraVersion}"
      fi

      ALIASES="${aliases}"
      if [[ -n "\${ALIASES}" ]] && [[ "\${ALIASES}" != "null" ]]; then
        BUILD_IMAGE_NAME+=",\${ALIASES}"
      fi
      if [[ "${version}" != "latest" && "${latest}" == "true" ]]; then
        BUILD_IMAGE_NAME+=",${name}:latest"
      fi

      echo "Building and shipping image to \${BUILD_IMAGE_NAME}"
      echo "Annotation: \${IMAGE_ANNOTATIONS}"
      echo "Context: ${context}"
      echo "Dockerfile: ${dockerfile}"
      echo "Dockerfile path: \$(dirname "\$(readlink -f ${dockerfile})")"
      echo "filename: \$(basename "${dockerfile}")"
      echo "buildArgs: ${buildArgs}"
      echo "push: ${push}"
      echo "latest: ${latest}"

      BUILD_ARGS="${buildArgs}"
      [[ "\${BUILD_ARGS}" == "null" ]] && BUILD_ARGS=""
      
      if [[ ! -f  "${dockerfile}" ]]; then
        echo "Dockerfile not found: ${dockerfile}"
        exit 1
      fi

      buildctl --addr="${buildkitdAddr}" \
        build \
          --progress=plain \
          --frontend=dockerfile.v0 \
          --local context="${context}" \
          --local dockerfile="\$(dirname "\$(readlink -f ${dockerfile})")" \
          --output type=image,\\"name=\${BUILD_IMAGE_NAME}\\",push=${push}\${IMAGE_ANNOTATIONS} \
          --opt filename="\$(basename "${dockerfile}")" \
          \${BUILD_ARGS}

    """
    }

}
