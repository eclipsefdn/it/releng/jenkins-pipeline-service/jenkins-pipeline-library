package org.eclipsefdn.jenkins.container

import org.jenkinsci.plugins.workflow.cps.CpsScript

import org.eclipsefdn.jenkins.utils.Config

/**
 * ContainerConfig represents the configuration for a Docker container in the Jenkins pipeline.
 * This class facilitates the management of configuration parameters for container-related operations.
 */
@SuppressWarnings(['SerializableClassMustDefineSerialVersionUID', 'CompileStatic'])
public class ContainerConfig implements Serializable {

  // Default configuration for the container
  @SuppressWarnings(['FieldName'])
  private final static Map DEFAULT_CONFIG = [
    credentialsId: null,            // registry credentials
    registry: 'docker.io',          // Default registry
    name: null,                     // Image name (must be specified)
    version: 'latest',              // Default image version
    extraVersions: [],              // Allows to tag image with different version
    aliases: '',                    // Push to other different aliases
    dockerfile: 'Dockerfile',       // Path to the Dockerfile
    context: '.',                   // Build context
    push: true,                     // Enable push by default
    buildArg: '',                   // Additional build arguments
    annotation: true,               // Enable image annotations by default
    latest: false,                  // Enable latest tag
    debug: false,                   // Default debug mode
    kubeAgentYmlFile: null          // Kubernetes agent configuration YAML file
  ]

  // The Cps script associated with the Jenkins workflow
  CpsScript workflowScript

  // Configuration utility to merge default and provided configurations
  Config configUtils

  // Container configuration
  Map config

  /**
   * Constructor for ContainerConfig.
   *
   * @param workflowScript The CpsScript associated with the Jenkins workflow.
   * @param givenConfig    Additional configuration provided for the container (optional).
   */
  ContainerConfig(CpsScript workflowScript, Map givenConfig = [:]) {
    this.workflowScript = workflowScript
    configUtils = new Config()
    config = configUtils.mergeMaps(DEFAULT_CONFIG, givenConfig)
  }

}
