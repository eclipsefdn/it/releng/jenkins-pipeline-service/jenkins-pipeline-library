package org.eclipsefdn.jenkins.preview

class DeployKind {
  static final String PRODUCTION_DEPLOY = "productionDeploy"
  static final String BRANCH_DEPLOY = "branchDeploy"
  static final String CHANGE_REQUEST_PREVIEW = "changeRequestPreview"
}
