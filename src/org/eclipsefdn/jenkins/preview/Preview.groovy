package org.eclipsefdn.jenkins.preview

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset

import com.cloudbees.groovy.cps.NonCPS
import org.jenkinsci.plugins.workflow.cps.CpsScript
import org.jenkinsci.plugins.workflow.cps.EnvActionImpl

public class Preview implements Serializable {

  CpsScript workflowScript

  Map config

  Preview(CpsScript workflowScript, Map givenConfig = [:]) {
    this.workflowScript = workflowScript
    config = mergeMaps(DEFAULT_CONFIG, givenConfig)

    config.safeAppName = config.appName.replaceAll("\\.", '-').replaceAll('[:/]', '-')
    config.container.name = config.container.name ?: "docker.io/eclipsefdn/${config.safeAppName}"
  }

  private static Map DEFAULT_CONFIG = [
    /**
    * REQUIRED. The name of the application. It will be used to derive many generated objects names.
    * May be altered to remove/change unwanted characters in some places.
    */
    appName: null,

    safeAppName: null,

    /**
    * The name of the branch that is containing the code to be deployed to ${productionDomain}
    */
    productionBranchName: 'main',

    /**
    * REQUIRED. The URL the production website will be reachable from.
    */
    productionDomain: null,

    /**
    * Whether change set (GitHub PR, Gitlab ${previewBranchesRegex && MR) or branches matching ${previewBranchesRegex} will end up
    * being deployed
    */
    deployPreviews: true,

    /**
    * Preview domain where previews (and branches if ${branchDomain} ${previewBranchesRegex && is not set and ${previewBranchesRegex}
    * matches current branch) will be deployed
    */
    previewDomain: 'eclipsecontent.org',

    /**
    * Regex of branches name for which a deployment should happen (e.g, 'staging|testing-[0-9]+' or '.*'     previewBranchesRegex: && for all)
    */
    previewBranchesRegex: null,

    /**
    * When specified, ${previewBranchesRegex && previews of branches matching ${previewBranchesRegex} will be deployed to
    * ${BRANCH_NAME}.${branchDomain} rather than the default ${BRANCH_NAME}--${appName}.${previewDomain}
    */
    branchDomain: null,

    /**
    * By default, build once a week to keep up with parents images updates
    */
    cronExpression: 'H H * * H',

    /**
    * Allows to activated debug in bash scripts, and some groovy scripts
    */
    debug: false,

    container: [
      /**
      * Default to docker.io/eclipsefdn/${safeAppName} if not set. Do not include image tag, it will be
      * dynamically computed at build time.
      */
      name: null,
      version: null,

      dockerfile: null,
      buildArgs: null,
      annotation: true,

      /**
      * The Jenkins credentials id to push ${name}
      */
      registryCredentialsId: '04264967-fea0-40c2-bf60-09af5aeba60f',
    ],

    deployment: [
      /**
      * The root location at which the website should be made available
      */
      locationPath: '/',

      nginxHttpPort: 8080,

      /**
      * The default nginx configuration that will be copied in the deployed image. If emtpy,
      * defaults to library's resources/org/eclipsefdn/jamstack/nginx/default.conf
      * Note that if you specify yours but don't use 8080 as the public port, you will need
      * to edit ${nginxHttpPort} as well.
      */
      nginxServerConf: null,

      /* Computed at build time. Not settable via configuration */
      dateTimeUTC: null,
      domain: null,
      kind: null,

      containerImageNameWithTag: null,

      authBasic: false,
      authBasicSecretPath: '/etc/nginx/htpasswd',
      secretPath: ''
    ],

    build: [
      /**
      * The kubernetes pod template file that will be used as the main agent for this build.
      * If emtpy, defaults to library's resources/org/eclipsefdn/jamstack/hugo/agent.yml.
      * If overriden, it must contain a 'kubectl' container for handling deployment (with
      * kubectl installed), a 'containertools' container for building and pushing container
      * images (with buildctl and crane installed). Site will be build inside the 'jamstack'
      * container, that should have all the tools required to run ${script}.
      */
      kubeAgentYmlFile: null,

      /**
      * The image inside which the ${script} will be executed.
      * See https://github.com/EclipseFdn/dockerfiles/tree/master/hugo-node for list of tags
      */
      containerImage: 'eclipsefdn/hugo-node:latest',

      containerBuildCpuLimits: '2',
      containerBuildMemoryLimits: '2Gi',

      containerJnlpCpuLimits: '2',
      containerJnlpMemoryLimits: '2Gi',

      containerToolsCpuLimits: '500m',
      containerToolsMemoryLimits: '256Mi',

      containerKubeCpuLimits: '500m',
      containerKubeMemoryLimits: '256Mi',

      destinationFolder: './public/',
      /**
      * The build script that will be executed. If empty, default to library's one
      * resources/org/eclipsefdn/jamstack/hugo/build.sh
      * It will be given 1 argument, ${destinationFolder}, the path of the output folder
      * (that will be copied to docker image at a later stage).
      */
      script: null
    ],

    kubernetes: [
      /**
      * The kubernetes namespace to deploy the site into
      */
      namespace: 'foundation-internal-webdev-jamstack-sites',

      credentialsId: 'ci-bot-okd-c1-token',

      apiServerUrl: 'https://api.okd-c1.eclipse.org:6443'
    ],

    scm: [
      credentialsId: null,
      changeId: null,
      branchName: null,
      repoHost: null,
      repoId: null,
      botId: null
    ]
  ]

  @NonCPS
  private static Map mergeMaps(Map onto, Map... overrides) {
    if (!overrides) {
      return onto
    }
    else if (overrides.length == 1) {
      overrides[0]?.each { k, v ->
            if (v instanceof Map && onto[k] instanceof Map) {
          mergeMaps((Map) onto[k], (Map) v)
            }
            else {
          onto[k] = v
            }
      }
      return onto
    }
    return overrides.inject(onto, { acc, override -> mergeMaps(acc, override ?: [:]) })
  }

  public void computeScmInfo(String credentialsId) {
    config.scm.credentialsId = credentialsId
    config.scm.changeId = workflowScript.env.CHANGE_ID
    config.scm.branchName = workflowScript.env.BRANCH_NAME
    config.scm.commitId = workflowScript.env.GIT_BASE_COMMIT

    def matcherHttp = workflowScript.env.GIT_URL =~ '/^[^/]+://([^/]+)/(.*)$/'
    def matcherGit = workflowScript.env.GIT_URL =~ /^git@([^:]+):(.+?)(?:\.*)?$/
    def repoHost = ""
    def repoId = ""
    def http = true
    if (matcherHttp.find()) {
      repoHost = matcherHttp.group(1)
      repoId = matcherHttp.group(2)
    } else if (matcherGit.find()) {
      repoHost = matcherGit.group(1)
      repoId = matcherGit.group(2)
      http = false
    } else {
        throw new RuntimeException("❌ Format d'URL Git invalide : ${gitUrl}")
    }
    config.scm.repoHost = repoHost
    config.scm.repoId = repoId.replaceAll('.git', '')

    if (http) {
      workflowScript.withCredentials([
        workflowScript.usernamePassword(credentialsId: credentialsId,
        passwordVariable: '__UNUSED', usernameVariable: 'BOT_ID')]) {
        config.scm.botId = workflowScript.env.BOT_ID
        }
    }
  }

  public void computeDeploymentInfo() {
    config.deployment.dateTimeUTC = LocalDateTime.now(ZoneId.of(ZoneOffset.UTC.toString())).toString()
    config.kubernetes.deploymentName = "${config.safeAppName}--${config.scm.branchName}".toLowerCase()

    if (config.container.version == null) {
      String branchNameCleanFormat = config.scm.branchName.replaceAll('[/_]', '-')
      String containerImageTag = "${branchNameCleanFormat}--${workflowScript.env.GIT_BASE_COMMIT.substring(0, 7)}".toLowerCase()
      if (config.productionBranchName == config.scm.branchName) {
        containerImageTag = 'latest'
      }
      config.container.version = "${containerImageTag}"
    }
    config.deployment.containerImageNameWithTag = "${config.container.name}:${config.container.version}"

    if (config.scm.branchName.equals(config.productionBranchName)) {
      config.deployment.domain = config.deployment.domain == null ? config.productionDomain : config.deployment.domain
      config.deployment.kind = DeployKind.PRODUCTION_DEPLOY
    } else if (config.deployPreviews && config.scm.changeId != null) {
      config.deployment.domain = "preview-${config.scm.changeId}--${config.safeAppName}.${config.previewDomain}"
      config.deployment.kind = DeployKind.CHANGE_REQUEST_PREVIEW
      config.productionDomain = config.deployment.domain
    } else if (config.deployPreviews && config.previewBranchesRegex && config.scm.branchName.matches(config.previewBranchesRegex)) {
      if (!''.equals(config.branchDomain)) {
        config.deployment.domain = "${config.scm.branchName}.${config.branchDomain}"
      } else {
        config.deployment.domain = "${config.scm.branchName}--${config.safeAppName}.${config.previewDomain}"
      }
      config.deployment.kind = DeployKind.BRANCH_DEPLOY
      config.productionDomain = config.deployment.domain
    } else {
      config.deployment.domain = null
      config.deployment.kind = null
    }
  }

}
