package org.eclipsefdn.jenkins.scm

import org.eclipsefdn.jenkins.preview.DeployKind
import org.eclipsefdn.jenkins.preview.Preview

class ChangeRequestNotifier implements Serializable {

  GitForge gitForge
  String kind
  String commentHeader

  def setGitForge(GitForge gitForge, Map config) {
    this.gitForge = gitForge
    this.kind = config.deployment.kind
    this.commentHeader = "<!-- org.eclipsefdn.cbi.preview ${config.scm.branchName}--${config.scm.commitId} -->"
  }

  def notifyPreviewBuildStart(String body) {
    if (kind == DeployKind.CHANGE_REQUEST_PREVIEW) {
      def mostRecentCommentForSameCommit = gitForge.listChangeRequestComments().find{ it.body.startsWith(commentHeader) }

      if (mostRecentCommentForSameCommit != null) {
        gitForge.editChangeRequestComment(mostRecentCommentForSameCommit.id, body)
      } else {
        gitForge.createChangeRequestComment("${commentHeader}\n${body}")
      }
    }
  }

  def notifyPreviewBuildEnd(long commentId, String body) {
    if (kind == DeployKind.CHANGE_REQUEST_PREVIEW) {
      gitForge.editChangeRequestComment(commentId, "${commentHeader}\n${body}")
    }
  }
}
