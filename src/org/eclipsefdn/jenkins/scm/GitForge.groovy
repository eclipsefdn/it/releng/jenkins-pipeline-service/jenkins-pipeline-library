package org.eclipsefdn.jenkins.scm

import java.io.File

import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import org.jenkinsci.plugins.workflow.cps.CpsScript
import org.eclipsefdn.jenkins.preview.Preview

public abstract class GitForge implements Serializable {

  public abstract Object listChangeRequestComments();
  public abstract Object editChangeRequestComment(long commentId, String body);
  public abstract Object createChangeRequestComment(String body);

  public static GitForge newGitForge(CpsScript workflowScript, Map scm) {
    if ("github.com".equals(scm.repoHost)) {
      return new GitHub(workflowScript)
    } else if ("gitlab.eclipse.org".equals(scm.repoHost)) {
      return new GitLab(workflowScript, scm.credentialsId, scm.repoId, scm.changeId)
    } else {
      throw new IllegalArgumentException("Expected 'github.com' or 'gitlab.eclipse.org' as repoHost, got " + scm.repoHost)
    }
  }
}

public class GitHub extends GitForge {

  CpsScript workflowScript

  GitHub(CpsScript workflowScript) {
    this.workflowScript = workflowScript
  }

  public Object createChangeRequestComment(String body) {
    workflowScript.pullRequest.comment(body)
  }

  public Object editChangeRequestComment(long commentId, String body) {
    workflowScript.pullRequest.editComment(commentId, body)
  }

  public Object listChangeRequestComments() {
    workflowScript.pullRequest.comments
  }
}

public class GitLab extends GitForge {

  CpsScript workflowScript
  String credentialsId
  String repoId
  String changeRequestId

  GitLab(CpsScript workflowScript, String credentialsId, String repoId, String changeRequestId) {
    this.workflowScript = workflowScript
    this.credentialsId = credentialsId
    this.repoId = URLEncoder.encode(repoId, "UTF-8")
    this.changeRequestId = changeRequestId
  }

  private String createHeaders() {
    workflowScript.withCredentials([
      workflowScript.usernamePassword(credentialsId: this.credentialsId,
        passwordVariable: 'GITLAB_MR_COMMENT_TOKEN',
        usernameVariable: '__UNUSED')]) {
        File tmpFile = File.createTempFile("headers",".tmp")
        workflowScript.writeFile (file: tmpFile.getAbsolutePath(),
          text: """\
          Authorization: Bearer ${workflowScript.env.GITLAB_MR_COMMENT_TOKEN}
          Content-Type: application/json
          """.stripIndent()
        )
        return tmpFile.getAbsolutePath()
    }
  }

  private String createBodyPayload(String body) {
    File tmpFile = File.createTempFile("bodyPayload",".json")
    workflowScript.writeFile (file: tmpFile.getAbsolutePath(),
      text: JsonOutput.toJson([body: body]))
    return tmpFile.getAbsolutePath()
  }

  public Object createChangeRequestComment(String body) {
      def headers = createHeaders()
      def payload = createBodyPayload(body)

      def output = workflowScript.sh (script: """#!/usr/bin/env bash
        set -exuo pipefail

        curl -fsSL -H@"${headers}" -X POST --data @"${payload}" \
          "https://gitlab.eclipse.org/api/v4/projects/${repoId}/merge_requests/${changeRequestId}/notes"

        rm -f "${headers}" "${payload}"
      """, returnStdout: true)

      return new JsonSlurper().parseText(output)
  }

  public Object editChangeRequestComment(long commentId, String body) {
    def headers = createHeaders()
    def payload = createBodyPayload(body)

    def output = workflowScript.sh (script: """#!/usr/bin/env bash
      set -exuo pipefail

      curl -fsSL -H@"${headers}" -X PUT --data @"${payload}" \
        "https://gitlab.eclipse.org/api/v4/projects/${repoId}/merge_requests/${changeRequestId}/notes/${commentId}"

      rm -f "${headers}" "${payload}"
    """, returnStdout: true)

    return new JsonSlurper().parseText(output)
  }

  /**
  * Returns all comments for the given MR (List of https://docs.gitlab.com/ee/api/notes.html#merge-requests)
  * Default returns the 16 most recent comments.
  */
  public Object listChangeRequestComments() {
    def headers = createHeaders()

    def output = workflowScript.sh (script: """#!/usr/bin/env bash
      set -exuo pipefail

      curl -fsSL -H@"${headers}" \
        "https://gitlab.eclipse.org/api/v4/projects/${repoId}/merge_requests/${changeRequestId}/notes?per_page=50"

      rm -f "${headers}"
    """, returnStdout: true)

    new JsonSlurper().parseText(output)
  }
}