package org.eclipsefdn.jenkins.utils

import com.cloudbees.groovy.cps.NonCPS

/**
 * Config provides utility methods for merging maps.
 */
@SuppressWarnings(['SerializableClassMustDefineSerialVersionUID', 'CompileStatic'])
public class Config implements Serializable {

  /**
   * Merges maps, combining the provided maps with the base map.
   *
   * @param onto      The base map onto which the other maps will be merged.
   * @param overrides Additional maps to merge onto the base map.
   * @return The merged map.
   */
  @NonCPS
  private static Map mergeMaps(Map onto, Map... overrides) {
    if (!overrides) {
      return onto
    } else if (overrides.length == 1) {
      overrides[0]?.each { k, v ->
        if (v instanceof Map && onto[k] instanceof Map) {
          mergeMaps((Map) onto[k], (Map) v)
        } else {
          onto[k] = v
        }
      }
      return onto
    }
    return overrides.inject(onto, { acc, override -> mergeMaps(acc, override ?: [:]) })
  }

}
