/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation [and others]
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

import org.eclipsefdn.jenkins.container.ContainerConfig
import org.eclipsefdn.jenkins.container.ContainerBuild

def call(Map givenConfig = [:]) {
  ContainerConfig containerConfig = new ContainerConfig(this, givenConfig)
  Map config = containerConfig.config.clone()
  ContainerBuild containerBuild = new ContainerBuild(this)
  containerBuild.login(config.credentialsId, config.registry, config.name, config.debug)
  containerBuild.build(
    config.name,
    config.version,
    config.extraVersions,
    config.aliases,
    config.dockerfile,
    config.context,
    config.push,
    config.buildArgs,
    config.annotation,
    config.latest,
    config.debug)
}
