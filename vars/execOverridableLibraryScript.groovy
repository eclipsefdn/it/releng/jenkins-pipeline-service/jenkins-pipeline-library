def call(Map givenConfig = [:]) {
  def defaultConfig = [
    "userResource": "",
    "libraryResource": "",
    "templateBindings": [:],
    "args": [],
    "returnStdout": false,
    "returnStatus": false,
  ]
  def effectiveConfig = defaultConfig + givenConfig

  def scriptName = "${env.JOB_BASE_NAME}__" + sh(
    script: """#!/usr/bin/env bash
      set -euo pipefail
      basename ${effectiveConfig.libraryResource}
    """,
    returnStdout: true
  ).trim()

  writeFile (
    file: "${scriptName}",
    text: loadOverridableResource(effectiveConfig)
  )

  sh (
    script: """#!/usr/bin/env bash
      set -euo pipefail
      chmod a+x "${scriptName}"
      "./${scriptName}" "${effectiveConfig.args.join('" "')}"
    """,
    returnStdout: effectiveConfig.returnStdout,
    returnStatus: effectiveConfig.returnStatus
  )
}