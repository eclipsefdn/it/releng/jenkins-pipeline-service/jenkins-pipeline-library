/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation [and others]
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

import groovy.json.JsonOutput

import org.eclipsefdn.jenkins.scm.ChangeRequestNotifier
import org.eclipsefdn.jenkins.scm.GitForge
import org.eclipsefdn.jenkins.container.ContainerBuild
import org.eclipsefdn.jenkins.preview.Preview

def call(Map givenConfig = [:]) {
  def preview = new Preview(this, givenConfig)
  def config = preview.config

  def changeRequestNotifier = new ChangeRequestNotifier()
  def hugoContainerBuild = new ContainerBuild(this)
  def comment = null

  pipeline {
    agent {
      kubernetes {
        yaml loadOverridableResource(libraryResource: 'org/eclipsefdn/jamstack/hugo/agent.yml',
          userResource: config.build.kubeAgentYmlFile,
          templateBindings: config
        )
      }
    }

    options {
      quietPeriod(2)
      parallelsAlwaysFailFast()
      timeout(time: 60, unit: 'MINUTES')
      buildDiscarder(logRotator(numToKeepStr: '10'))
      disableConcurrentBuilds(abortPrevious: true)
    }

    triggers {
      // build once a week to keep up with parents images updates
      cron(config.cronExpression)
    }

    environment {
      HOME = "${env.JENKINS_AGENT_WORKDIR}"
    }

    stages {
      stage('Compute build variables') {
        steps {
          script {
            sh """
              #!/usr/bin/env bash
              [[ "${config.debug}" == "true" ]] && printenv
              echo "credentialsId: ${scm.userRemoteConfigs[0].credentialsId}"
              exit 0
            """

            def baseCommit = ''
            def latestCommit = sh(label: 'Get previous commit', script: "git rev-parse HEAD", returnStdout: true)?.trim()
            def previousCommit = sh(label: 'Get previous commit', script: "git rev-parse HEAD^", returnStdout: true)?.trim()
            println 'latestCommit: ' + latestCommit
            println 'previousCommit: ' + previousCommit
            if(env?.CHANGE_ID){
              baseCommit = previousCommit
            } else {
              baseCommit = latestCommit
            }
            env.GIT_BASE_COMMIT = baseCommit

            println 'GIT_BASE_COMMIT: ' + env.GIT_BASE_COMMIT

            preview.computeScmInfo(scm.userRemoteConfigs[0].credentialsId)
            preview.computeDeploymentInfo()

            println 'Deployment config: '
            println JsonOutput.prettyPrint(JsonOutput.toJson(config))

            def gitForge = GitForge.newGitForge(this, config.scm)
            changeRequestNotifier.setGitForge(gitForge, config)

            comment = changeRequestNotifier.notifyPreviewBuildStart("""
              :hourglass_flowing_sand: Deploy Preview for *${config.appName}* started.

              :mag: Inspect the [build logs](${env.BUILD_URL}console)
            """.stripIndent())
          }
        }
      }

      stage('Build Hugo site') {
        steps {
          container('build') {
            execOverridableLibraryScript(
              libraryResource: 'org/eclipsefdn/jamstack/hugo/build.sh',
              userResource: config.build.script,
              args: [config.build.destinationFolder, "https://${config.productionDomain}${config.deployment.locationPath}"]
            )
          }
        }
      }

      stage('Build Docker image') {
        environment {
          DOCKERFILE = "${env.JOB_BASE_NAME}.Dockerfile"
          NGINX_CONF = "${env.JOB_BASE_NAME}-nginx-default.conf"
        }

        steps {
          container('containertools') {
            script {
              String nginxTemplate = config.deployment.authBasic ?
                'org/eclipsefdn/jamstack/nginx/default-auth.conf' : 'org/eclipsefdn/jamstack/nginx/default.conf'

              writeFile file: env.NGINX_CONF,
                text: loadOverridableResource(
                  libraryResource: nginxTemplate,
                  userResource: config.deployment.nginxServerConf,
                  templateBindings: config
                )

              config.deployment.nginxServerConf = env.NGINX_CONF

              writeFile file: env.DOCKERFILE,
                text: loadOverridableResource(
                  libraryResource: 'org/eclipsefdn/jamstack/hugo/Dockerfile',
                  userResource: config.container.dockerfile,
                  templateBindings: config
                )

              hugoContainerBuild.login(
                config.container.registryCredentialsId,
                null,
                config.container.name.toString(),
                config.debug)

              hugoContainerBuild.build(
                config.container.name,
                config.container.version,
                null, // extraVersions
                null, // aliases
                env.DOCKERFILE,
                env.WORKSPACE,
                 true,
                config.container.buildArgs,
                config.container.annotation,
                false, //latest
                config.debug)
            }
          }
        }
      }

      stage('Deployment and cleanup') {
        parallel {
          stage('Cleanup previews') {
            when {
              expression { config.productionBranchName == config.scm.branchName }
            }
            steps {
              container('kubectl') {
                withKubeConfig([credentialsId: config.kubernetes.credentialsId, serverUrl: config.kubernetes.apiServerUrl]) {
                  sh """#!/usr/bin/env bash
                    set -euo pipefail

                    [[ "${config.debug}" == "true" ]] && set -x

                    labelPrefix="preview.cbi.eclipse.org"
                    appSelector="\${labelPrefix}/app-name=${config.safeAppName},\${labelPrefix}/deploy-kind=changeRequestPreview"

                    previewsMetadata="\$(kubectl get deployment -n ${config.kubernetes.namespace} -l "\${appSelector}" -o json | jq -c '.items|map(.metadata)[]')"

                    while read -r line; do
                      lastDeployDatetime="\$(jq -r '.annotations["'"\${labelPrefix}"'/last-deploy-datetime"]'<<<"\${line}")"
                      previewBranchName="\$(jq -r '.labels["'"\${labelPrefix}"'/branch-name"]'<<<"\${line}")"
                      if [[ \$(date -d "\${lastDeployDatetime}" +%s) -lt \$(date -d "60 days ago" +%s) ]]; then
                        echo "Preview \${previewBranchName} of ${config.safeAppName} has expired (60+ days old). Deleting..."
                        kubectl delete -n "${config.kubernetes.namespace}" deployment,service,route -l "\${appSelector},\${labelPrefix}/branch-name=\${previewBranchName}"
                      elif [[ \$(date -d "\${lastDeployDatetime}" +%s) -lt \$(date -d "30 days ago" +%s) ]]; then
                        echo "Preview \${previewBranchName} of ${config.safeAppName} is stalled. Warning..."
                        # stale
                      fi
                    done<<<"\${previewsMetadata[@]}"
                  """
                }
              }
            }
          }
          stage('Deploy to kubernetes cluster') {
            when {
              expression { config.deployment.kind != null }
            }
            steps {
              container('kubectl') {
                writeFile file: 'jsonnetfile.json', text: loadOverridableResource(
                  libraryResource: 'org/eclipsefdn/jamstack/deployment/jsonnetfile.json'
                )
                writeFile file: 'jsonnetfile.lock.json', text: loadOverridableResource(
                  libraryResource: 'org/eclipsefdn/jamstack/deployment/jsonnetfile.lock.json'
                )
                writeFile file: 'kube-deploy.jsonnet', text: loadOverridableResource(
                  libraryResource: 'org/eclipsefdn/jamstack/deployment/kube-deploy.jsonnet'
                )
                writeFile file: 'eclipsefdn-preview-config.json',
                  text: JsonOutput.toJson(config)

                withKubeConfig([credentialsId: config.kubernetes.credentialsId, serverUrl: config.kubernetes.apiServerUrl]) {
                  sh """#!/usr/bin/env bash
                    set -euo pipefail

                    [[ "${config.debug}" == "true" ]] && set -x

                    # Suppress git warning about default initial branch name on `jb` call
                    git config --global init.defaultBranch main

                    jb install

                    deployment_exists="\$(kubectl get "deployment/${config.kubernetes.deploymentName}" -n "${config.kubernetes.namespace}" --ignore-not-found)"

                    [[ "${config.debug}" == "true" ]] && jsonnet --tla-code config='import "eclipsefdn-preview-config.json"' kube-deploy.jsonnet
                    jsonnet --tla-code config='import "eclipsefdn-preview-config.json"' kube-deploy.jsonnet | kubectl apply -f -

                    if [[ -n "\${deployment_exists}" ]]; then
                      kubectl rollout restart deployment "${config.kubernetes.deploymentName}" -n "${config.kubernetes.namespace}"
                    fi

                    kubectl rollout status -w -n "${config.kubernetes.namespace}" "deployment/${config.kubernetes.deploymentName}"
                  """
                }
              }
            }
          }
        }
      }
    }

    post {
      success {
        script {
          if (comment != null) {
            // comment is not null only for change request
            changeRequestNotifier.notifyPreviewBuildEnd(comment.id, """
                :white_check_mark: Deploy Preview for *${config.appName}* is ready [:stopwatch: ${currentBuild.durationString.minus(' and counting')}]

                :mag: Inspect the [build logs](${env.BUILD_URL}console)

                :whale: Test locally `docker run -it --rm -p 18181:${config.deployment.nginxHttpPort} ${config.deployment.containerImageNameWithTag}` and open to http://localhost:18181

                :rocket: Browse the preview: https://${config.productionDomain}${config.deployment.locationPath}
              """.stripIndent()
            )
          }
          println "You can browse the site at https://${config.productionDomain}${config.deployment.locationPath}"
        }
      }
      failure {
        script {
          if (comment != null) {
            changeRequestNotifier.notifyPreviewBuildEnd(comment.id, """
                :x: Deploy Preview for *${config.appName}* has failed [:stopwatch: ${currentBuild.durationString.minus(' and counting')}]

                :mag: Inspect the [build logs](${env.BUILD_URL}console)

                :repeat_one: Restart a [deploy now](${env.JOB_URL}build)
              """.stripIndent()
            )
          }
        }
      }
      aborted {
        script {
          if (comment != null) {
            changeRequestNotifier.notifyPreviewBuildEnd(comment.id, """
                :o: Deploy Preview for *${config.appName}* has been canceled [:stopwatch: ${currentBuild.durationString.minus(' and counting')}]

                :mag: Inspect the [build logs](${env.BUILD_URL}console)

                :repeat_one: Restart a [deploy now](${env.JOB_URL}build)
              """.stripIndent()
            )
          }
        }
      }
    }
  }
}
