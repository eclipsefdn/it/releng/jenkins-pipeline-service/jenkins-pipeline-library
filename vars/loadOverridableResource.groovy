def call(Map givenConfig = [:]) {
  def defaultConfig = [
    "userResource": "",
    "libraryResource": "",
    "templateBindings": [:]
  ]
  def effectiveConfig = defaultConfig + givenConfig

  if (!effectiveConfig.libraryResource?.trim()) {
    throw new IllegalArgumentException("Argument 'libraryResource' is mandatory")
  }

  def resourceContent
  if (effectiveConfig.userResource?.trim()) {
    println "readTrusted: ${effectiveConfig.userResource}"
    resourceContent = readTrusted(effectiveConfig.userResource)
    println "readTrusted: ${effectiveConfig.userResource} OK!"
  } else {
    resourceContent = libraryResource effectiveConfig.libraryResource
  }
  
  if (effectiveConfig.templateBindings.size() > 0) {
    resourceContent = resourceContent.replace("\\", "\\\\")
    resourceContent = resourceContent.replace("\$", "\\\$")

    def templateEngine = new groovy.text.SimpleTemplateEngine()
    def tpl = templateEngine.createTemplate(resourceContent)
    resourceContent = tpl.make(new HashMap(effectiveConfig.templateBindings)).toString()
    resourceContent = resourceContent.replace("\\\$", "\$")
  } else {
    resourceContent
  }
  echo "Final resourceContent: ${resourceContent}"
  return resourceContent
}
